create schema if not exists users;

create table if not exists tbl_user_account
(
    id              bigserial not null
                    constraint tbl_user_account_pkey
                    primary key,
    username        varchar(36) unique not null,
    email           varchar(36) unique not null,
    first_name      varchar(64) not null,
    last_name       varchar(64) not null,
    gender          varchar(8),
    enabled         boolean default true,
    created_on      timestamp with time zone,
    created_by      varchar(36),
    modified_on     timestamp with time zone,
    modified_by     varchar(36),
    reference       uuid not null
)

