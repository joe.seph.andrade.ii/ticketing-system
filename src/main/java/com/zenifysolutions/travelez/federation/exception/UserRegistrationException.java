package com.zenifysolutions.travelez.federation.exception;

import com.zenifysolutions.travelez.common.exception.TravelEZRuntimeException;
import com.zenifysolutions.travelez.common.model.enums.ApiResponseCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserRegistrationException extends TravelEZRuntimeException {

    public UserRegistrationException(ApiResponseCode errorCode, Throwable throwable) {
        super(errorCode, throwable);
    }
}
