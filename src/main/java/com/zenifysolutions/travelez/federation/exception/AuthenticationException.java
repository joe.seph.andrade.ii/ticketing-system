package com.zenifysolutions.travelez.federation.exception;

import com.zenifysolutions.travelez.common.exception.TravelEZRuntimeException;
import com.zenifysolutions.travelez.common.model.enums.ApiResponseCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AuthenticationException extends TravelEZRuntimeException {

    public AuthenticationException(ApiResponseCode errorCode) {
        super(errorCode);
    }

    public AuthenticationException(ApiResponseCode errorCode, Throwable throwable) {
        super(errorCode, throwable);
    }
}
