package com.zenifysolutions.travelez.federation.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@EqualsAndHashCode
@ToString(onlyExplicitlyIncluded = true)
@Entity
@Table(schema = "users", name = "tbl_user_account")
public class User extends BaseEntity {

    @ToString.Include
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false, columnDefinition = "serial")
    private Long id;

    @Getter
    @Setter
    @ToString.Include
    @Size(max = 36)
    @NotNull
    @Column(unique = true)
    private String username;

    @Getter
    @Setter
    @ToString.Include
    @Size(max = 80)
    @NotNull
    @Column(unique = true)
    private String email;

    @Getter
    @Setter
    @ToString.Include
    @Size(max = 64)
    @NotNull
    private String firstName;

    @Getter
    @Setter
    @ToString.Include
    @Size(max = 64)
    @NotNull
    private String lastName;

    @Getter
    @Setter
    @ToString.Include
    private boolean enabled;
}
