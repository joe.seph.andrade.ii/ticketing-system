package com.zenifysolutions.travelez.federation.service;

import com.zenifysolutions.travelez.federation.model.UserRegistrationDto;
import com.zenifysolutions.travelez.federation.model.keycloak.KeycloakAccessTokenRequest;
import com.zenifysolutions.travelez.federation.model.keycloak.KeycloakRefreshTokenRequest;
import com.zenifysolutions.travelez.federation.model.keycloak.KeycloakTokenResponse;

public interface KeycloakService {

    KeycloakTokenResponse authenticate(KeycloakAccessTokenRequest request);

    KeycloakTokenResponse refreshToken(KeycloakRefreshTokenRequest request);

    UserRegistrationDto createUser(UserRegistrationDto userRegistration);
}
