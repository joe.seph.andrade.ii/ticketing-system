package com.zenifysolutions.travelez.federation.service;

import com.zenifysolutions.travelez.common.model.enums.ApiResponseCode;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;

public abstract class AbstractSecurityService {

    protected <T> HttpEntity<MultiValueMap<String, String>> createFormUrlEncodedHttpEntity(T request) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-Type", MediaType.APPLICATION_FORM_URLENCODED_VALUE);

        return new HttpEntity<>(resolveFormUrlEncodedHttpEntity(request), httpHeaders);
    }

    protected abstract <T> MultiValueMap<String, String> resolveFormUrlEncodedHttpEntity(T request);
    protected abstract ApiResponseCode mapHttpExceptionsToResponseCode(HttpClientErrorException e);
}
