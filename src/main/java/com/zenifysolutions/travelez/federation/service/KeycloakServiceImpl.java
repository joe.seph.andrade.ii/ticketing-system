package com.zenifysolutions.travelez.federation.service;

import com.zenifysolutions.travelez.common.model.enums.ApiResponseCode;
import com.zenifysolutions.travelez.federation.entity.User;
import com.zenifysolutions.travelez.federation.exception.AuthenticationException;
import com.zenifysolutions.travelez.common.util.RestExceptionMapper;
import com.zenifysolutions.travelez.federation.exception.UserRegistrationException;
import com.zenifysolutions.travelez.federation.feign.KeycloakClient;
import com.zenifysolutions.travelez.federation.model.UserRegistrationDto;
import com.zenifysolutions.travelez.federation.model.keycloak.KeycloakAccessTokenRequest;
import com.zenifysolutions.travelez.federation.model.keycloak.KeycloakApiError;
import com.zenifysolutions.travelez.federation.model.keycloak.KeycloakRefreshTokenRequest;
import com.zenifysolutions.travelez.federation.model.keycloak.KeycloakTokenResponse;
import com.zenifysolutions.travelez.federation.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.keycloak.representations.account.UserRepresentation;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;

import javax.validation.constraints.NotNull;

import static com.zenifysolutions.travelez.federation.Constants.Keycloak.*;

@Service
@Slf4j
public class KeycloakServiceImpl extends AbstractSecurityService
        implements KeycloakService {

    private static final String GRANT_TYPE_PASSWORD = "password";
    private static final String GRANT_TYPE_REFRESH_TOKEN = "refresh_token";

    @Autowired
    private RestExceptionMapper restExceptionMapper;

    @Autowired
    private KeycloakClient keycloakClient;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Value("${keycloak.realm}")
    private String realm;

    @Value("${keycloak.resource}")
    private String keycloakClientId;

    @Value("${keycloak.credentials.secret}")
    private String keycloakClientSecret;

    @Override
    public KeycloakTokenResponse authenticate(@NonNull KeycloakAccessTokenRequest request) {
        try {
            log.debug("Initiating authentication for user [{}]", request.getUsername());
            return keycloakClient.authenticate(realm, resolveFormUrlEncodedHttpEntity(request));
        } catch (HttpClientErrorException e) {
            log.error("Error occurred while requesting for an access token for user {}", e.getMessage());
            throw new AuthenticationException(mapHttpExceptionsToResponseCode(e), e);
        }
    }

    @Override
    public KeycloakTokenResponse refreshToken(KeycloakRefreshTokenRequest request) {
        try {
            return keycloakClient.refreshToken(realm, resolveFormUrlEncodedHttpEntity(request));
        } catch (HttpClientErrorException e) {
            log.error("Error occurred while refreshing access token: {}", e.getMessage());
            throw new AuthenticationException(mapHttpExceptionsToResponseCode(e), e);
        }
    }

    @Override
    public UserRegistrationDto createUser(@NotNull UserRegistrationDto userRegistration) {
        var keycloakUserRepresentation = modelMapper.map(userRegistration, UserRepresentation.class);

        log.debug("Creating user account for [{}]:[{}]", userRegistration.getUsername(), userRegistration.getEmail());
        keycloakClient.createUser(realm, keycloakUserRepresentation);

        try {
            var user = modelMapper.map(userRegistration, User.class);

            log.debug("Persisting user account [{}], enabled:[{}]", user.getUsername(), user.isEnabled());
            return modelMapper.map(userRepository.save(user), UserRegistrationDto.class);
        } catch (Exception e) {
            log.error("Error occurred while creating user: {}", e.getMessage());
            throw new UserRegistrationException(ApiResponseCode.USER_ALREADY_EXISTS, e);
        }
    }

    @Override
    protected <T> MultiValueMap<String, String> resolveFormUrlEncodedHttpEntity(T request) {
        if (request instanceof KeycloakAccessTokenRequest) {
            return resolveFormUrlEncodedHttpEntity((KeycloakAccessTokenRequest) request);
        } else if (request instanceof KeycloakRefreshTokenRequest) {
            return resolveFormUrlEncodedHttpEntity((KeycloakRefreshTokenRequest) request);
        }

        return null;
    }

    @Override
    protected ApiResponseCode mapHttpExceptionsToResponseCode(HttpClientErrorException e) {
        var responseCode = ApiResponseCode.INVALID_CREDENTIALS;
        var apiError = restExceptionMapper.map(e, KeycloakApiError.class);
        if (apiError != null) {
            if (StringUtils.equals("Invalid refresh token", apiError.getErrorDescription())) {
                responseCode = ApiResponseCode.INVALID_TOKEN;
            }
        }

        return responseCode;
    }

    private MultiValueMap<String, String> resolveFormUrlEncodedHttpEntity(KeycloakAccessTokenRequest request) {
        var requestBody = new LinkedMultiValueMap<String, String>();
        requestBody.add(CLIENT_ID, keycloakClientId);
        requestBody.add(CLIENT_SECRET, keycloakClientSecret);
        requestBody.add(USERNAME, request.getUsername());
        requestBody.add(PASSWORD, request.getPassword());
        requestBody.add(GRANT_TYPE, GRANT_TYPE_PASSWORD);

        return requestBody;
    }

    private MultiValueMap<String, String> resolveFormUrlEncodedHttpEntity(KeycloakRefreshTokenRequest request) {
        var requestBody = new LinkedMultiValueMap<String, String>();
        requestBody.add(CLIENT_ID, keycloakClientId);
        requestBody.add(CLIENT_SECRET, keycloakClientSecret);
        requestBody.add(REFRESH_TOKEN, request.getRefreshToken());
        requestBody.add(GRANT_TYPE, GRANT_TYPE_REFRESH_TOKEN);

        return requestBody;
    }
}
