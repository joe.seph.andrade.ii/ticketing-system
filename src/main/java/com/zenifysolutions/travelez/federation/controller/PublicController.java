package com.zenifysolutions.travelez.federation.controller;

import com.zenifysolutions.travelez.common.model.ApiResponse;
import com.zenifysolutions.travelez.common.model.enums.ApiResponseCode;
import com.zenifysolutions.travelez.common.util.ApiResponseResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/public")
public class PublicController {

    @Autowired
    private ApiResponseResolver responseResolver;

    @GetMapping("/ping")
    ResponseEntity<ApiResponse> ping() {
        return ResponseEntity.ok(responseResolver.resolve(ApiResponseCode.SUCCESS, "Successfully established connection"));
    }
}
