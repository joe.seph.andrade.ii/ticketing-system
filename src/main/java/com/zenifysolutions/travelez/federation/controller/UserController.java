package com.zenifysolutions.travelez.federation.controller;

import com.zenifysolutions.travelez.federation.model.UserRegistrationDto;
import com.zenifysolutions.travelez.federation.service.KeycloakService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private KeycloakService keycloakService;

    @PostMapping("/register")
    ResponseEntity<UserRegistrationDto> registerUser(@RequestBody UserRegistrationDto userRegistration) {
        var registeredUser = keycloakService.createUser(userRegistration);
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(registeredUser);
    }
}
