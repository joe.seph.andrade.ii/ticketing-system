package com.zenifysolutions.travelez.federation.controller;

import com.zenifysolutions.travelez.federation.model.keycloak.KeycloakAccessTokenRequest;
import com.zenifysolutions.travelez.federation.model.keycloak.KeycloakRefreshTokenRequest;
import com.zenifysolutions.travelez.federation.model.keycloak.KeycloakTokenResponse;
import com.zenifysolutions.travelez.federation.service.KeycloakService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/secure")
public class SecureController {

    @Autowired
    private KeycloakService keycloakService;

    @PostMapping("/auth")
    ResponseEntity<KeycloakTokenResponse> authenticate(@RequestBody KeycloakAccessTokenRequest tokenRequest) {
        var tokenResponse = keycloakService.authenticate(tokenRequest);
        return ResponseEntity.ok(tokenResponse);
    }

    @PostMapping("/token")
    ResponseEntity<KeycloakTokenResponse> refreshToken(@RequestBody KeycloakRefreshTokenRequest tokenRequest) {
        var tokenResponse = keycloakService.refreshToken(tokenRequest);
        return ResponseEntity.ok(tokenResponse);
    }
}
