package com.zenifysolutions.travelez.federation.controller.advice;

import com.zenifysolutions.travelez.common.exception.*;
import com.zenifysolutions.travelez.common.util.ApiResponseResolver;
import com.zenifysolutions.travelez.federation.exception.AuthenticationException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Slf4j
@RestControllerAdvice
public class GlobalControllerAdvice extends ResponseEntityExceptionHandler {

    @Autowired
    private ApiResponseResolver apiResponseResolver;

    @ExceptionHandler(value = InvalidRequestException.class)
    protected ResponseEntity<Object> handleBadRequest(TravelEZRuntimeException exception, WebRequest request) {
        log.error("Request failed due to error: {} {}", exception.getErrorCode(), exception.getMessage());
        return handleExceptionInternal(exception,
                apiResponseResolver.resolve(exception.getErrorCode()),
                new HttpHeaders(),
                HttpStatus.BAD_REQUEST,
                request);
    }

    @ExceptionHandler(value = AuthenticationException.class)
    protected ResponseEntity<Object> handleUnauthorized(TravelEZRuntimeException exception, WebRequest request) {
        log.error("Request failed due to error: {} {}", exception.getErrorCode(), exception.getMessage());
        return handleExceptionInternal(exception,
                apiResponseResolver.resolve(exception.getErrorCode()),
                new HttpHeaders(),
                HttpStatus.UNAUTHORIZED,
                request);
    }

    @ExceptionHandler(value = AccessRestrictedException.class)
    protected ResponseEntity<Object> handleForbidden(TravelEZRuntimeException exception, WebRequest request) {
        log.error("Request failed due to error: {} {}", exception.getErrorCode(), exception.getMessage());
        return handleExceptionInternal(exception,
                apiResponseResolver.resolve(exception.getErrorCode()),
                new HttpHeaders(),
                HttpStatus.FORBIDDEN,
                request);
    }

    @ExceptionHandler(value = ResourceNotFoundException.class)
    protected ResponseEntity<Object> handleNotFound(TravelEZRuntimeException exception, WebRequest request) {
        log.error("Request failed due to error: {} {}", exception.getErrorCode(), exception.getMessage());
        return handleExceptionInternal(exception,
                apiResponseResolver.resolve(exception.getErrorCode()),
                new HttpHeaders(),
                HttpStatus.NOT_FOUND,
                request);
    }

    @ExceptionHandler(value = AlreadyExistException.class)
    protected ResponseEntity<Object> handleConflict(TravelEZRuntimeException exception, WebRequest request) {
        log.error("Request failed due to error: {} {}", exception.getErrorCode(), exception.getMessage());
        return handleExceptionInternal(exception,
                apiResponseResolver.resolve(exception.getErrorCode()),
                new HttpHeaders(),
                HttpStatus.CONFLICT,
                request);
    }

    @ExceptionHandler(value = TravelEZRuntimeException.class)
    protected ResponseEntity<Object> handleInternalServer(TravelEZRuntimeException exception, WebRequest request) {
        log.error("Request failed due to error: {} {}", exception.getErrorCode(), exception.getMessage());
        return handleExceptionInternal(exception,
                apiResponseResolver.resolve(exception.getErrorCode()),
                new HttpHeaders(),
                HttpStatus.INTERNAL_SERVER_ERROR,
                request);
    }
}
