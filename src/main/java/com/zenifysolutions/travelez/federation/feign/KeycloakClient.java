package com.zenifysolutions.travelez.federation.feign;

import com.zenifysolutions.travelez.federation.config.feign.KeycloakFeignClientConfig;
import com.zenifysolutions.travelez.federation.model.keycloak.KeycloakTokenResponse;
import org.keycloak.representations.account.UserRepresentation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(value = "feign-keycloak",
        url = "${feign.client.keycloak.auth-server-url}",
        configuration = KeycloakFeignClientConfig.class)
public interface KeycloakClient {

    @PostMapping("/realms/{realm}/protocol/openid-connect/token")
    KeycloakTokenResponse authenticate(@PathVariable("realm") String realm,
                                       @RequestBody MultiValueMap<String, String> tokenRequest);

    @PostMapping("/realms/{realm}/protocol/openid-connect/token")
    KeycloakTokenResponse refreshToken(@PathVariable("realm") String realm,
                                       @RequestBody MultiValueMap<String, String> tokenRequest);

    @PostMapping("/admin/realms/{realm}/users")
    void createUser(@PathVariable("realm") String realm,
                    @RequestBody UserRepresentation userRepresentation);
}
