package com.zenifysolutions.travelez.federation.config.feign;

import feign.RequestInterceptor;
import feign.codec.ErrorDecoder;
import feign.okhttp.OkHttpClient;
import lombok.extern.slf4j.Slf4j;
import org.keycloak.KeycloakPrincipal;
import org.keycloak.adapters.RefreshableKeycloakSecurityContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.context.SecurityContextHolder;

@Configuration
@Slf4j
public class KeycloakFeignClientConfig {

    @Bean
    public OkHttpClient client() {
        return new OkHttpClient();
    }

    @Bean
    public RequestInterceptor authInterceptor() {
        return requestTemplate -> {
            if (SecurityContextHolder.getContext().getAuthentication() != null
                && SecurityContextHolder.getContext().getAuthentication().getCredentials() instanceof RefreshableKeycloakSecurityContext) {
                KeycloakPrincipal keycloakPrincipal = (KeycloakPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

                requestTemplate.header("Authorization", String.format("Bearer %s", keycloakPrincipal.getKeycloakSecurityContext().getTokenString()));
            }
        };
    }

    @Bean
    public ErrorDecoder errorDecoder() {
        return new KeycloakFeignErrorDecoder();
    }
}
