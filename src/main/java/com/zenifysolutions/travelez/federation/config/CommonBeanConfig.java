package com.zenifysolutions.travelez.federation.config;

import com.zenifysolutions.travelez.common.feign.mapper.FeignApiResponseCodeMapper;
import com.zenifysolutions.travelez.common.util.ApiResponseResolver;
import com.zenifysolutions.travelez.common.util.RestExceptionMapper;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CommonBeanConfig {

    @Bean
    public ApiResponseResolver apiResponseResolver() {
        return new ApiResponseResolver();
    }

    @Bean
    public RestExceptionMapper restExceptionMapper() {
        return new RestExceptionMapper();
    }

    @Bean
    public FeignApiResponseCodeMapper feignApiResponseCodeMapper() {
        return new FeignApiResponseCodeMapper();
    }

    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }
}
