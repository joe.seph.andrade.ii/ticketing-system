package com.zenifysolutions.travelez.federation.config.feign;

import com.zenifysolutions.travelez.common.exception.*;
import com.zenifysolutions.travelez.common.feign.mapper.FeignApiResponseCodeMapper;
import com.zenifysolutions.travelez.common.model.enums.ApiResponseCode;
import com.zenifysolutions.travelez.federation.exception.AuthenticationException;
import feign.Response;
import feign.codec.ErrorDecoder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class KeycloakFeignErrorDecoder implements ErrorDecoder {

    @Autowired
    private FeignApiResponseCodeMapper feignApiResponseCodeMapper;

    @Override
    public Exception decode(String methodKey, Response response) {
        log.debug("{} feign client returned an HTTP {} response", methodKey, response.status());
        switch (response.status()){
            case 400:
                return new InvalidRequestException(feignApiResponseCodeMapper.getApiResponseCode(methodKey, response.status()));
            case 401:
                return new AuthenticationException(feignApiResponseCodeMapper.getApiResponseCode(methodKey, response.status()));
            case 403:
                return new AccessRestrictedException(feignApiResponseCodeMapper.getApiResponseCode(methodKey, response.status()));
            case 404:
                return new ResourceNotFoundException(feignApiResponseCodeMapper.getApiResponseCode(methodKey, response.status()));
            case 409:
                return new AlreadyExistException(feignApiResponseCodeMapper.getApiResponseCode(methodKey, response.status()));
            default:
                return new TravelEZRuntimeException(ApiResponseCode.REQUEST_FAILED);
        }
    }
}
