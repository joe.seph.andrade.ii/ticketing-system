package com.zenifysolutions.travelez.federation.model.keycloak;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class KeycloakApiError {

    private String error;

    @JsonProperty("error_description")
    private String errorDescription;
}
