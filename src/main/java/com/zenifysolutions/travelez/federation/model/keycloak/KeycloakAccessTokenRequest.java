package com.zenifysolutions.travelez.federation.model.keycloak;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class KeycloakAccessTokenRequest {

    private String username;

    private String password;
}
