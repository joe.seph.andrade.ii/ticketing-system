package com.zenifysolutions.travelez.federation.model.keycloak;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class KeycloakRefreshTokenRequest {

    @JsonProperty("refresh_token")
    private String refreshToken;
}
