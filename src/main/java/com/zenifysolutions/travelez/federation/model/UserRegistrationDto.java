package com.zenifysolutions.travelez.federation.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;

@Data
@EqualsAndHashCode
public class UserRegistrationDto {

    @NotBlank
    private String username;

    @NotBlank
    private String email;

    private String firstName;

    private String lastName;

    private boolean enabled;
}
