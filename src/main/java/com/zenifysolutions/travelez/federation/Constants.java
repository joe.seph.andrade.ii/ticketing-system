package com.zenifysolutions.travelez.federation;

public final class Constants {

    public interface Keycloak {
        String CLIENT_ID = "client_id";
        String CLIENT_SECRET = "client_secret";
        String GRANT_TYPE = "grant_type";
        String REFRESH_TOKEN = "refresh_token";
        String USERNAME = "username";
        String PASSWORD = "password";
    }
}
