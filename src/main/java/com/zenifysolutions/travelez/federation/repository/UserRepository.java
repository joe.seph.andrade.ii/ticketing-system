package com.zenifysolutions.travelez.federation.repository;

import com.zenifysolutions.travelez.federation.entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {
}
