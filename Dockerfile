FROM adoptopenjdk/openjdk11:alpine-slim
ARG JAR_FILE
ARG SPRING_PROFILE
COPY target/${JAR_FILE} /usr/app/app.jar
EXPOSE 22911
ENTRYPOINT ["java", "-Dspring.profiles.active=${SPRING_PROFILE}", "-jar", "/usr/app/app.jar"]